package com.gautam.SAXParser;

public class OneNews {
	private String mName;
	private String mLink;
	private String mImageURL;

	public void setmName(String mName) {
		this.mName = mName;
	}

	public void setmLink(String mLink) {
		this.mLink = mLink;
	}

	public void setmImageURL(String mImageURL) {
		this.mImageURL = mImageURL;
	}

	public String putmName() {
		return mName;
	}

	public String putmLink() {
		return mLink;
	}

	public String putmImageURL() {
		return mImageURL;
	}
}