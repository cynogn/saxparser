package com.gautam.SAXParser;

import java.util.ArrayList;

import android.util.Log;

/**
 * Data contains the links
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 */
public class Data {
	private static Data instance = null;
	private static ArrayList<String> links = new ArrayList<String>();

	public Data() {

	}

	/**
	 * Singleton Object implementation
	 * 
	 * @return
	 */
	public static Data getInstance() {
		if (instance == null) {
			instance = new Data();
		}
		return instance;
	}

	public void addLink(String content) {
		links.add(content);
		Log.v("ADDED!DATA ", "links are" + links.toString());
		Log.v("Count", "links are" + links.size());
	}

	public void removeTop(String content) {
		links.remove(content.trim());
		Log.v("REMOVED!DATA", "links are" + links.toString());
		Log.v("count", "links are" + links.size());
	}

}